const gulp = require('gulp');
const sloc = require('gulp-sloc');
const gutil = require('gulp-util');
const child_process = require('child_process');

gulp.task('lines', () => {
  gulp.src(['src/**/*.js'])
    .pipe(sloc());
});

gulp.task('disk-usage', (done) => {
  child_process.exec('du -sh | cut -f1 -d"\t"', (error, result) => {
    if(!error) {
      // Remove trailing newline from the result
      const usage = result.trim();
      // Print the disk usage
      gutil.log('Disk usage: ' + usage);
      // Tell Gulp that we're done
      done();
    } else {
      // Tell Gulp that we encountered an error
      done(error);
    }
  });
});
