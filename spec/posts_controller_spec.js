const request = require('request');
const Promise = require('pinkie-promise');

const models = require('../src/models');
const app = require('../src/app');

const port = 3001;
const baseUrl = 'http://localhost:' + port;

describe('Post endpoints', () => {
  let server;

  beforeAll(() => {
    // Start the server
    server = app.listen(port);
  });

  afterAll(() => {
    // Stop the server
    server.close();
  });

  describe('GET /posts', () => {
    it('returns a list of posts', (done) => {
      // Create list of fake posts
      const posts = ['Fake post', 'Another fake post'];

      // Create a mock implementation of the findAll function
      spyOn(models.Post, 'findAll').and.callFake(() => {
        return new Promise((resolve) => {
          resolve(posts);
        });
      });

      // Send a request
      request.get(baseUrl + '/posts', (err, response, body) => {
        expect(err).toBeFalsy();
        expect(response).toBeOk();
        expect(response).toContainJson();
        expect(models.Post.findAll).toHaveBeenCalled();
        expect(body).toEqual(JSON.stringify(posts));
        done();
      });
    });

    it('returns posts older than the olderThan param', (done) => {
      // Create list of fake posts
      const posts = ['Old man post'];

      // Create a mock implementation of the findAll function
      spyOn(models.Post, 'findAll').and.callFake(() => {
        return new Promise((resolve) => {
          resolve(posts);
        });
      });

      // Send a request
      request.get(baseUrl + '/posts?olderThan=2016-01-01', (err, response, body) => {
        expect(err).toBeFalsy();
        expect(response).toBeOk();
        expect(response).toContainJson();
        expect(models.Post.findAll).toHaveBeenCalledWith(jasmine.objectContaining({
          where: { createdAt: { $lt: new Date('2016-01-01') } }
        }));
        expect(body).toEqual(JSON.stringify(posts));
        done();
      });
    });
  });

  describe('GET /posts/:postId', () => {
    it('returns the post with specified ID', (done) => {
      // Create fake posts
      const posts = { 42: 'Fake post #42' };

      // Create a mock implementation of the findById function
      spyOn(models.Post, 'findById').and.callFake((postId) => {
        return new Promise((resolve, reject) => {
          resolve(posts[postId]);
        });
      });

      // Send a request
      request.get(baseUrl + '/posts/42', (err, response, body) => {
        expect(err).toBeFalsy();
        expect(response).toBeOk();
        expect(response).toContainJson();
        expect(models.Post.findById).toHaveBeenCalled();
        expect(body).toEqual(JSON.stringify(posts[42]));
        done();
      });
    });
  });
});
